import React, { useRef } from 'react'
import Row from './Row'

import './index.css'

const Column = ({ data, styles = {}, height }) => {
    const { id, rows, title } = data
    const ref = useRef(null)

    return (
        <div id={id} style={{ ...(styles.column || {}), maxHeight: height, height: '100%' }}>
            <h1>{title}</h1>
            <div ref={ref} className='rows' style={styles.rows || {}}>
                {rows.map(row =>
                    <Row Component={row} />
                )}
            </div>
        </div>
    )    
}

export default Column