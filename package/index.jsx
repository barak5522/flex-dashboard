import React, { useRef, useEffect, useState } from 'react'
import Column from './Column'

import './index.css'

const dashbaord = ({ data, height, maxWidth }) => {
    const { columns, styles = {} } = data
    const ref = useRef(null)
    const [columnsElements, setColumns] = useState([])

    useEffect(() => {
        const elements = columns.map(column => <Column data={column} styles={styles} height={height} />)
        setColumns(elements)
    }, [columns, styles])

    useEffect(() => {
        if (ref.current) {
            const elementsArr = Array.prototype.slice.call(ref.current.childNodes)
            const width = elementsArr.reduce((width, element) => {
                return width + element.clientWidth                
            }, 0)
            if (width > maxWidth) {
                const shortElements = elementsArr.filter(element => element.offsetHeight < height)
                const elementsSortBySize = shortElements.sort((a,b)=> {
                    const aSize = a.offsetHeight * a.offsetWidth
                    const bSize = b.offsetHeight * b.offsetWidth
                    return aSize < bSize ? 1 : -1
                })

                let ids, firstElementHeight, secondElementHeight
                for (const index in elementsSortBySize) {
                    const element = elementsSortBySize[index]
                    let fitId = null
                    firstElementHeight = element.offsetHeight
                    secondElementHeight = height - firstElementHeight
                    const spaceLeft = secondElementHeight * element.offsetWidth
                    for (const index2 in elementsSortBySize) {
                        const element2 = elementsSortBySize[index2]
                        if (index >= index2) continue
                        const space = element2.offsetHeight * element2.offsetWidth
                        if (space <= spaceLeft) {
                            fitId = element2.id
                            break
                        }
                    }
                    if (fitId) {
                        ids = [element.id, fitId]
                        break
                    }
                }
                if (ids) {
                    const newElements = columnsElements.filter(element => !ids.includes(element.props.data?.id))
                    let firstColumn = columnsElements.find(element => ids[0] === element.props.id || element.props.data.id)
                    if (firstColumn.props.id) {
                        const lastChild = firstColumn.props.children[firstColumn.props.children.length] 
                        lastChild.props.height = lastChild.props.height - secondElementHeight
                    } else {
                        firstColumn = <Column data={firstColumn.props.data} styles={styles} height={firstColumn.props.height - secondElementHeight} />    
                    }
                    const secondColumn = columnsElements.find(element => ids[1] === element.props.data?.id)
                    const mergeColumn = <div id={ids[0]} className='mergeColumn'>
                        {firstColumn}
                        <Column data={secondColumn.props.data} styles={styles} height={secondElementHeight} />
                    </div>
                    setColumns([mergeColumn, ...newElements])
                }
            }
        }
    }, [columnsElements, height, maxWidth, styles])

    return (
        <div className='wrap' style={{ maxHeight: height }}>
            <div ref={ref} className='body' style={styles.dashbaord || {}} >
                {columnsElements}
            </div>
        </div>
    )
}

export default dashbaord